/*
 * This file is part of VisualCrypto.
 * Copyright (C) 2018  Jan Wabbersen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import model.VisualCryptoModel;
import view.SegmentVCFrame;
import view.VisualCryptoView;

/**
 * This class manages the interactions between the GUI and the model.
 * 
 * The shares of the common (pixel-based) visual cryptography are called slides.
 * The shares of the segment-based visual cryptography are called segment shares.
 */
public class VisualCryptoController {
    private final VisualCryptoModel model;
    private final VisualCryptoView view;
    private SegmentVCFrame segmentFrame;
    
    /**
     * Constructor - sets model and view.
     * @param model the model.
     * @param view the view.
     */
    public VisualCryptoController(VisualCryptoModel model, VisualCryptoView view) {
        this.model = model;
        this.view = view;
    }
    
    /**
     * Starts the controller by adding listeners.
     */
    public void control() {
        addListeners();
    }
    
    /**
     * Helper method to set listeners.
     */
    private void addListeners() {
        view.setLoadImageButtonListener(new LoadImageButtonListener());
        view.setCreateRandomSlideButtonListener(new CreateRandomSlideButtonListener());
        view.setCreateSecondSlideButtonListener(new CreateSecondSlideButtonListener());
        view.setComputeResultButtonListener(new ComputeResultButtonListener());
        view.setSaveImageButtonListener(new SaveImageButtonListener());
        view.setLeftArrowButtonListener(new LeftArrowButtonListener());
        view.setRightArrowButtonListener(new RightArrowButtonListener());
        view.setMenuItemListener(new MenuItemListener());
        // TODO: Move ButtonListeners of SegmentVCFrame here...
    }
    
    /**
     * Listener class for menu items.
     */
    class MenuItemListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            switch (ae.getActionCommand()) {
                case VisualCryptoView.MENU_ITEM_SEGMENT:
                    segmentFrame = view.createSegmentFrame();
                    segmentFrame.setCreateButtonListener(
                            new SegmentCreateShareButtonListener());
                    segmentFrame.setSaveButtonListener(
                            new SegmentSaveShareButtonListener());
                    break;
            }
        }
    }
    
    /* ------------- Pixel-based visual cryptography ------------- */
    
    /**
     * Listener for the "create" button for random slides.
     */
    class CreateRandomSlideButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (model.getNumberOfRandomSlides() == 3) {
                view.showErrorDialog("Only 3 slides allowed.");
                return;
            }
            model.setSuperpixelSize(view.getSuperpixelSize());
            try {
                model.createRandomSlide();
            }
            catch (IllegalStateException e) {
                view.showErrorDialog("Please load an image first!");
                return;
            }
            view.setRandomSlide(model.getCurrRandomSlide());
            view.setRandomSlideLabel(model.getNumberOfCurrRandomSlide(), 
                                     model.getNumberOfRandomSlides());
        }
    }
    
    /**
     * Listener for the "create" button for second slides.
     */
    class CreateSecondSlideButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (model.getNumberOfSecondSlides() == 3) {
                view.showErrorDialog("Only 3 slides allowed.");
                return;
            }
            try {
                model.createSecondSlide();
            }
            catch (IllegalStateException e) {
                view.showErrorDialog("The dimensions of the current random slide "
                        + "does not mesh with the dimensions of the current "
                        + "image (according to the superpixel size)!");
                return;
            }
            catch (ArrayIndexOutOfBoundsException e) {
                view.showErrorDialog("Image or random slide is missing!\nPlease "
                        + "make sure to have an image and a random slide.");
                return;
            }
            view.setSecondSlide(model.getCurrSecondSlide());
            view.setSecondSlideLabel(model.getNumberOfCurrSecondSlide(), 
                                     model.getNumberOfSecondSlides());
        }
    }
    
    /**
     * Listener for the loading images and slides.
     */
    class LoadImageButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            switch (ae.getActionCommand()) {
                case VisualCryptoView.ACTION_COMMAND_IMAGE:
                    if (model.getNumberOfImages() == 3) {
                        view.showErrorDialog("Only 3 images allowed.");
                        return;
                    }
                    break;
                case VisualCryptoView.ACTION_COMMAND_RANDOM_SHARE:
                    if (model.getNumberOfRandomSlides() == 3) {
                        view.showErrorDialog("Only 3 slides allowed.");
                        return;
                    }
                    break;
                case VisualCryptoView.ACTION_COMMAND_SECOND_SHARE:
                    if (model.getNumberOfSecondSlides() == 3) {
                        view.showErrorDialog("Only 3 slides allowed.");
                        return;
                    }
                    break;
            }
            File file = view.getFileFromOpenDialog();
            /* Do nothing if no file was selected. */
            if (file == null) {
                return;
            }
            BufferedImage image = model.readImageFromFile(file);
            if (image == null) {
                view.showErrorDialog("Error.\nFile could not be read.");
                return;
            }
            switch (ae.getActionCommand()) {
                case VisualCryptoView.ACTION_COMMAND_IMAGE:
                    model.addImage(image);
                    view.setImage(model.getCurrImage());
                    view.setImageLabel(model.getNumberOfCurrImage(), 
                                       model.getNumberOfImages());
                    break;
                case VisualCryptoView.ACTION_COMMAND_RANDOM_SHARE:
                    model.addRandomSlide(image);
                    view.setRandomSlide(model.getCurrRandomSlide());
                    view.setRandomSlideLabel(model.getNumberOfCurrRandomSlide(), 
                                             model.getNumberOfRandomSlides());
                    break;
                case VisualCryptoView.ACTION_COMMAND_SECOND_SHARE:
                    model.addSecondSlide(image);
                    view.setSecondSlide(model.getCurrSecondSlide());
                    view.setSecondSlideLabel(model.getNumberOfCurrSecondSlide(), 
                                             model.getNumberOfSecondSlides());
                    break;
            }
        }
    }
    
    /**
     * Listener for all LeftArrowButtons.
     */
    class LeftArrowButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ae) {
            switch (ae.getActionCommand()) {
                case VisualCryptoView.ACTION_COMMAND_IMAGE:
                    int imageNumber = model.getNumberOfCurrImage();
                    if (imageNumber >= 2 && imageNumber <= 3) {
                        model.setNumberOfCurrImage(imageNumber - 1);
                        view.setImage(model.getCurrImage());
                        view.setImageLabel(model.getNumberOfCurrImage(), 
                                           model.getNumberOfImages());
                    }
                    break;
                case VisualCryptoView.ACTION_COMMAND_RANDOM_SHARE:
                    int randomSlideNumber = model.getNumberOfCurrRandomSlide();
                    if (randomSlideNumber >= 2 && randomSlideNumber <= 3) {
                        model.setNumberOfCurrRandomSlide(randomSlideNumber - 1);
                        view.setRandomSlide(model.getCurrRandomSlide());
                        view.setRandomSlideLabel(model.getNumberOfCurrRandomSlide(), 
                                                 model.getNumberOfRandomSlides());
                    }
                    break;
                case VisualCryptoView.ACTION_COMMAND_SECOND_SHARE:
                    int secondSlideNumber = model.getNumberOfCurrSecondSlide();
                    if (secondSlideNumber >= 2 && secondSlideNumber <= 3) {
                        model.setNumberOfCurrSecondSlide(secondSlideNumber - 1);
                        view.setSecondSlide(model.getCurrSecondSlide());
                        view.setSecondSlideLabel(model.getNumberOfCurrSecondSlide(), 
                                                 model.getNumberOfSecondSlides());
                    }
                    break;
            }
        }
    }
    
    /**
     * Listener for all RightArrowButtons.
     */
    class RightArrowButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ae) {
            switch (ae.getActionCommand()) {
                case VisualCryptoView.ACTION_COMMAND_IMAGE:
                    int imageNumber = model.getNumberOfCurrImage();
                    if (imageNumber >= 1 && imageNumber <= 2 && 
                        imageNumber < model.getNumberOfImages()) {
                        model.setNumberOfCurrImage(imageNumber + 1);
                        view.setImage(model.getCurrImage());
                        view.setImageLabel(model.getNumberOfCurrImage(), 
                                           model.getNumberOfImages());
                    }
                    break;
                case VisualCryptoView.ACTION_COMMAND_RANDOM_SHARE:
                    int randomSlideNumber = model.getNumberOfCurrRandomSlide();
                    if (randomSlideNumber >= 1 && randomSlideNumber <= 2 && 
                        randomSlideNumber < model.getNumberOfRandomSlides()) {
                        model.setNumberOfCurrRandomSlide(randomSlideNumber + 1);
                        view.setRandomSlide(model.getCurrRandomSlide());
                        view.setRandomSlideLabel(model.getNumberOfCurrRandomSlide(), 
                                                 model.getNumberOfRandomSlides());
                    }
                    break;
                case VisualCryptoView.ACTION_COMMAND_SECOND_SHARE:
                    int secondSlideNumber = model.getNumberOfCurrSecondSlide();
                    if (secondSlideNumber >= 1 && secondSlideNumber <= 2 && 
                        secondSlideNumber < model.getNumberOfSecondSlides()) {
                        model.setNumberOfCurrSecondSlide(secondSlideNumber + 1);
                        view.setSecondSlide(model.getCurrSecondSlide());
                        view.setSecondSlideLabel(model.getNumberOfCurrSecondSlide(), 
                                                 model.getNumberOfSecondSlides());
                    }
                    break;
            }
        }
    }
    
    /**
     * Listener for save buttons for images and slides.
     */
    class SaveImageButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            BufferedImage image = null;
            try {
                switch (ae.getActionCommand()) {
                    case VisualCryptoView.ACTION_COMMAND_IMAGE:
                        image = model.getCurrImage();
                        break;
                    case VisualCryptoView.ACTION_COMMAND_RANDOM_SHARE:
                        image = model.getCurrRandomSlide();
                        break;
                    case VisualCryptoView.ACTION_COMMAND_SECOND_SHARE:
                        image = model.getCurrSecondSlide();
                        break;
                    case VisualCryptoView.ACTION_COMMAND_RESULT_SHARE:
                        image = model.getResultSlide();
                        break;
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {
                view.showErrorDialog("There is no slide to save!");
                return;
            }
            if (image == null) {
                view.showErrorDialog("There is no slide to save!");
                return;
            }
            
            File file = view.getFileFromSaveDialog();
            if (file == null) {
                view.showErrorDialog("No file selected!");
                return;
            }
            try {
                ImageIO.write(image, "png", file);
            }
            catch (IllegalArgumentException e) {
                view.showErrorDialog("No file selected!");
                return;
            }
            catch (IOException e) {
                view.showErrorDialog("An error occured during writing!");
                return;
            }
        }
    }
    
    /**
     * Listener for the overlay button.
     */
    class ComputeResultButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            try {
                model.computeOverlayingSlide(view.getCheckedRandomSlides(), 
                                             view.getCheckedSecondSlides());
            }
            catch (IndexOutOfBoundsException e) {
                view.showErrorDialog("Not existent slides are chosen!");
                return;
            }
            catch (IllegalArgumentException e) {
                view.showErrorDialog(e.getMessage());
                return;
            }
            view.setResultSlide(model.getResultSlide());
        }
    }
    
    /* ------------- Segment-based visual cryptography ------------- */
    
    /**
     * Listener for all "create share" buttons.
     */
    class SegmentCreateShareButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            String TAN = segmentFrame.getTAN();
            switch (ae.getActionCommand()) {
                case SegmentVCFrame.ACTION_COMMAND_RANDOM_SHARE:
                    int segmentSize = segmentFrame.getSegmentSize();
                    if (TAN.length() < 1) {
                        segmentFrame.showErrorDialog("Please enter a TAN.");
                        return;
                    }
                    if (!TAN.matches("[0-9]+")) {
                        segmentFrame.showErrorDialog("Only digits allowed.");
                        return;
                    }
                    model.setSegmentSize(segmentSize);
                    model.setTANLength(TAN.length());
                    model.createRandomSegmentShare(TAN.length());
                    segmentFrame.setRandomShare(model.getRandomSegmentShare());
                    break;
                case SegmentVCFrame.ACTION_COMMAND_SECOND_SHARE:
                    if (TAN.length() < 1) {
                        segmentFrame.showErrorDialog("Please enter a TAN.");
                        return;
                    }
                    if (!TAN.matches("[0-9]+")) {
                        segmentFrame.showErrorDialog("Only digits allowed.");
                        return;
                    }
                    if (TAN.length() != model.getTANLength()) {
                        segmentFrame.showErrorDialog("TAN length is not "
                                + "consistent with the random slide or random "
                                + "slide is missing.");
                        return;
                    }
                    try {
                        model.createSecondSegmentShare(TAN);
                    }
                    catch (IllegalStateException e) {
                        segmentFrame.showErrorDialog("Random slide is "
                                + "missing.\nPlease create a random slide "
                                + "first.");
                    }
                    segmentFrame.setSecondShare(model.getSecondSegmentShare());
                    break;
                case SegmentVCFrame.ACTION_COMMAND_RESULT_SHARE:
                    if (model.getRandomSegmentShare() != null && 
                        model.getSecondSegmentShare() != null) {
                        model.createOverlaySegmentShare();
                        segmentFrame.setResultShare(model.getOverlaySegmentShare());
                    }
                    else {
                        segmentFrame.showErrorDialog("Random or second slide "
                                + "missing.");
                        return;
                    }
                    break;
            }
        }
    }
    
    /**
     * Listener for all "save share" buttons.
     */
    class SegmentSaveShareButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            BufferedImage image = null;
            try {
                switch (ae.getActionCommand()) {
                    case SegmentVCFrame.ACTION_COMMAND_RANDOM_SHARE:
                        image = model.getRandomSegmentShare();
                        break;
                    case SegmentVCFrame.ACTION_COMMAND_SECOND_SHARE:
                        image = model.getSecondSegmentShare();
                        break;
                    case SegmentVCFrame.ACTION_COMMAND_RESULT_SHARE:
                        image = model.getOverlaySegmentShare();
                        break;
                }
            }
            catch (ArrayIndexOutOfBoundsException e) {
                segmentFrame.showErrorDialog("There is no slide to save!");
                return;
            }
            if (image == null) {
                segmentFrame.showErrorDialog("There is no slide to save!");
                return;
            }
            
            File file = segmentFrame.getFileFromSaveDialog();
            if (file == null) {
                segmentFrame.showErrorDialog("No file selected!");
                return;
            }
            
            try {
                ImageIO.write(image, "png", file);
            }
            catch (IllegalArgumentException e) {
                segmentFrame.showErrorDialog("No file selected!");
                return;
            }
            catch (IOException e) {
                segmentFrame.showErrorDialog("An error occured during writing!");
                return;
            }
        }
    }
}