/*
 * VisualCrypto is a tool to demonstrate visual cryptography.
 * Copyright (C) 2018  Jan Wabbersen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package visualcrypto;

import javax.swing.SwingUtilities;

import controller.VisualCryptoController;
import model.VisualCryptoModel;
import view.VisualCryptoView;

/**
 * This class contains the main method for VisualCrypto.
 */
public class VisualCrypto {

    /**
     * @param args the command line arguments.
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                VisualCryptoModel model = new VisualCryptoModel();
                VisualCryptoView view = new VisualCryptoView();
                VisualCryptoController controller = new VisualCryptoController(
                        model, view);
                controller.control();
            }
        });
    }
}