/*
 * This file is part of VisualCrypto.
 * Copyright (C) 2018  Jan Wabbersen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.basic.BasicArrowButton;


/**
 * This class is responsible for most of the GUI.
 */
public class VisualCryptoView {
    /**
     * Minimum superpixel size: width = height = (value + 1) * 2.
     */
    public static final int MIN_SUPERPIXEL_SIZE = 0;
    /**
     * Maximum superpixel size: width = height = (value + 1) * 2.
     */
    public static final int MAX_SUPERPIXEL_SIZE = 3;
    /**
     * Start value for superpixel size: width = height = (value + 1) * 2.
     */
    public static final int START_SUPERPIXEL_SIZE = 0;
    public static final int SUPERPIXEL_TICK_SPACING = 1;
    
    /**
     * Width of all slide panels.
     */
    public static final int SLIDE_PANEL_WIDTH = 360;
    /**
     * Height of all slide panels.
     */
    public static final int SLIDE_PANEL_HEIGHT = 350;
    
    /**
     * Width of the panel scrollers.
     */
    public static final int SCROLLER_WIDTH = 300;
    /**
     * Height of the panel scrollers.
     */
    public static final int SCROLLER_HEIGHT = 220;
    
    public static final String ACTION_COMMAND_IMAGE = "image";
    public static final String ACTION_COMMAND_RANDOM_SHARE = "random_share";
    public static final String ACTION_COMMAND_SECOND_SHARE = "second_share";
    public static final String ACTION_COMMAND_RESULT_SHARE = "result_share";
    
    public static final String MENU_ITEM_PIXEL = "pixel_mode";
    public static final String MENU_ITEM_SEGMENT = "segment_mode";
    
    private final JFrame frame;
    private SegmentVCFrame segmentFrame;
    
    private JMenuBar menuBar;
    private JMenu modeMenu;
    private JMenuItem segmentModeMenuItem;
    
    /* Divide frame in left and right panel. */
    private final JPanel leftPanel;
    private final JPanel rightPanel;
    
    private JPanel imagePanel;
    private JButton loadImageButton;
    private JButton saveImageButton;
    private JScrollPane imageScroller;
    private ImagePanel image;
    private JLabel imageLabel;
    private BasicArrowButton imageLeftArrow;
    private BasicArrowButton imageRightArrow;
    
    private JPanel randSlidePanel;
    private JButton createRandSlideButton;
    private JButton loadRandSlideButton;
    private JButton saveRandSlideButton;
    private JScrollPane randSlideScroller;
    private ImagePanel randSlide;
    private JLabel randomSlideLabel;
    private BasicArrowButton randSlideLeftArrow;
    private BasicArrowButton randSlideRightArrow;
    
    private JPanel secondSlidePanel;
    private JButton createSecondSlideButton;
    private JButton loadSecondSlideButton;
    private JButton saveSecondSlideButton;
    private JScrollPane secondSlideScroller;
    private ImagePanel secondSlide;
    private JLabel secondSlideLabel;
    private BasicArrowButton secondSlideLeftArrow;
    private BasicArrowButton secondSlideRightArrow;
    
    private JPanel resultSlidePanel;
    private JButton computeResultButton;
    private JButton saveResultSlideButton;
    private JCheckBox[] randomSlidesCheckBoxes;
    private JCheckBox[] secondSlidesCheckBoxes;
    private JScrollPane resultScroller;
    private ImagePanel resultSlide;
    
    private JLabel sliderLabel;
    private JSlider superpixelSizeSlider;

    /**
     * Constructor for the initial GUI which includes the frame for the
     * common (pixel-based) visual cryptography.
     */
    public VisualCryptoView() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            this.showErrorDialog(e.getMessage());
        }
        frame = new JFrame("Pixel-based visual cryptography - VisualCrypto");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        setMenu();
        
        leftPanel = new JPanel();
        rightPanel = new JPanel();
        
        initLeftPanel();
        initRightPanel();
 
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        /* Disable divider of SplitPane. */
        splitPane.setEnabled(false);
        splitPane.setDividerSize(1);
        splitPane.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        splitPane.setLeftComponent(leftPanel);
        splitPane.setRightComponent(rightPanel);
        
        frame.add(splitPane);
        frame.pack();
        frame.setResizable(false);
        /* Place the frame in the center of the screen. */
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
    
    /**
     * Helper method to create the menu.
     */
    private void setMenu() {
        menuBar = new JMenuBar();
        modeMenu = new JMenu("Other Modes");
        segmentModeMenuItem = new JMenuItem("Segment Mode");
        segmentModeMenuItem.setActionCommand(MENU_ITEM_SEGMENT);
        modeMenu.add(segmentModeMenuItem);
        menuBar.add(modeMenu);
        frame.setJMenuBar(menuBar);
    }
    
    /**
     * Helper method to create the left panel.
     */
    private void initLeftPanel() {
        leftPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        JPanel settingsPanel = new JPanel();
        settingsPanel.setLayout(new BoxLayout(settingsPanel, BoxLayout.Y_AXIS));
        JLabel settingsLabel = new JLabel("Settings");
        settingsLabel.setFont(new Font("", Font.PLAIN, 20));
        sliderLabel = new JLabel("Superpixel Size: ");
        sliderLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        superpixelSizeSlider = new JSlider(JSlider.HORIZONTAL, 
                                           MIN_SUPERPIXEL_SIZE, 
                                           MAX_SUPERPIXEL_SIZE, 
                                           START_SUPERPIXEL_SIZE);
        superpixelSizeSlider.setAlignmentX(Component.LEFT_ALIGNMENT);
        Hashtable<Integer, JLabel> labels = new Hashtable<>();
        labels.put(0, new JLabel("2"));
        labels.put(1, new JLabel("4"));
        labels.put(2, new JLabel("6"));
        labels.put(3, new JLabel("8"));
        superpixelSizeSlider.setLabelTable(labels);
        superpixelSizeSlider.setPaintLabels(true);
        superpixelSizeSlider.setMajorTickSpacing(SUPERPIXEL_TICK_SPACING);
        superpixelSizeSlider.setPaintTicks(true);
        settingsPanel.add(settingsLabel);
        settingsPanel.add(Box.createVerticalStrut(20));
        settingsPanel.add(sliderLabel);
        settingsPanel.add(Box.createVerticalStrut(10));
        settingsPanel.add(superpixelSizeSlider);
        leftPanel.add(settingsPanel);
    }
    
    /**
     * Helper method to create the right panel.
     */
    private void initRightPanel() {
        rightPanel.setLayout(new GridLayout(2, 2, 10, 10));
        rightPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        
        /* Initialization of the image panel. */
        imagePanel = new JPanel();
        imagePanel.setPreferredSize(new Dimension(SLIDE_PANEL_WIDTH, 
                                                  SLIDE_PANEL_HEIGHT));
        imagePanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.GRAY), "Original Image"));
        imagePanel.setLayout(new BoxLayout(imagePanel, BoxLayout.Y_AXIS));
        loadImageButton = new JButton("Load Image");
        saveImageButton = new JButton("Save Image");
        loadImageButton.setActionCommand(ACTION_COMMAND_IMAGE);
        saveImageButton.setActionCommand(ACTION_COMMAND_IMAGE);
        imageLeftArrow = new BasicArrowButton(BasicArrowButton.WEST);
        imageRightArrow = new BasicArrowButton(BasicArrowButton.EAST);
        imageLeftArrow.setActionCommand(ACTION_COMMAND_IMAGE);
        imageRightArrow.setActionCommand(ACTION_COMMAND_IMAGE);
        imageLabel = new JLabel("       ");
        image = new ImagePanel();
        imageScroller = new JScrollPane(image);
        imageScroller.setPreferredSize(new Dimension(SCROLLER_WIDTH, 
                                                     SCROLLER_HEIGHT));
        imageScroller.setBorder(new LineBorder(Color.LIGHT_GRAY));
        
        JPanel imageButtonPanel = new JPanel();
        imageButtonPanel.add(loadImageButton);
        imageButtonPanel.add(saveImageButton);
        
        JPanel imageArrowPanel = new JPanel();
        imageArrowPanel.add(imageLeftArrow);
        imageArrowPanel.add(imageLabel);
        imageArrowPanel.add(imageRightArrow);
        imageArrowPanel.setMaximumSize(imageArrowPanel.getPreferredSize());
        
        JPanel imageScrollerPanel = new JPanel();
        imageScrollerPanel.add(imageScroller);
        
        imagePanel.add(imageButtonPanel);
        imagePanel.add(Box.createVerticalGlue());
        imagePanel.add(imageArrowPanel);
        imagePanel.add(imageScrollerPanel);
        
        rightPanel.add(imagePanel);
        
        /* Initialization of the random slide panel. */
        randSlidePanel = new JPanel();
        randSlidePanel.setPreferredSize(new Dimension(SLIDE_PANEL_WIDTH, 
                                                      SLIDE_PANEL_HEIGHT));
        randSlidePanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.GRAY), "Random Slide"));
        randSlidePanel.setLayout(new BoxLayout(randSlidePanel, BoxLayout.Y_AXIS));
        createRandSlideButton = new JButton("Create Slide");
        createRandSlideButton.setToolTipText("Create random slide for current "
                + "image with current superpixel size.");
        loadRandSlideButton = new JButton("Load Slide");
        saveRandSlideButton = new JButton("Save Slide");
        loadRandSlideButton.setActionCommand(ACTION_COMMAND_RANDOM_SHARE);
        saveRandSlideButton.setActionCommand(ACTION_COMMAND_RANDOM_SHARE);
        randSlideLeftArrow = new BasicArrowButton(BasicArrowButton.WEST);
        randSlideRightArrow = new BasicArrowButton(BasicArrowButton.EAST);
        randSlideLeftArrow.setActionCommand(ACTION_COMMAND_RANDOM_SHARE);
        randSlideRightArrow.setActionCommand(ACTION_COMMAND_RANDOM_SHARE);
        randomSlideLabel = new JLabel("       ");
        randSlide = new ImagePanel();
        randSlideScroller = new JScrollPane(randSlide);
        randSlideScroller.setPreferredSize(new Dimension(SCROLLER_WIDTH, 
                                                         SCROLLER_HEIGHT));
        randSlideScroller.setBorder(new LineBorder(Color.LIGHT_GRAY));
        
        JPanel randSlideButtonPanel = new JPanel();
        randSlideButtonPanel.add(createRandSlideButton);
        randSlideButtonPanel.add(loadRandSlideButton);
        randSlideButtonPanel.add(saveRandSlideButton);
        JPanel randSlideArrowPanel = new JPanel();
        randSlideArrowPanel.add(randSlideLeftArrow);
        randSlideArrowPanel.add(randomSlideLabel);
        randSlideArrowPanel.add(randSlideRightArrow);
        randSlideArrowPanel.setMaximumSize(randSlideArrowPanel.getPreferredSize());
        JPanel randSlideScrollerPanel = new JPanel();
        randSlideScrollerPanel.add(randSlideScroller);
        
        randSlidePanel.add(randSlideButtonPanel);
        randSlidePanel.add(Box.createVerticalGlue());
        randSlidePanel.add(randSlideArrowPanel);
        randSlidePanel.add(randSlideScrollerPanel);
        
        rightPanel.add(randSlidePanel);
        
        /* Initialization of the second slide panel. */
        secondSlidePanel = new JPanel();
        secondSlidePanel.setPreferredSize(new Dimension(SLIDE_PANEL_WIDTH, 
                                                        SLIDE_PANEL_HEIGHT));
        secondSlidePanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.GRAY), "Second Slide"));
        secondSlidePanel.setLayout(new BoxLayout(secondSlidePanel, 
                                                 BoxLayout.Y_AXIS));
        createSecondSlideButton = new JButton("Create Slide");
        createSecondSlideButton.setToolTipText("Create second slide for current "
                + "image and current random slide with current superpixel size.");
        loadSecondSlideButton = new JButton("Load Slide");
        saveSecondSlideButton = new JButton("Save Slide");
        loadSecondSlideButton.setActionCommand(ACTION_COMMAND_SECOND_SHARE);
        saveSecondSlideButton.setActionCommand(ACTION_COMMAND_SECOND_SHARE);
        secondSlideLeftArrow = new BasicArrowButton(BasicArrowButton.WEST);
        secondSlideRightArrow = new BasicArrowButton(BasicArrowButton.EAST);
        secondSlideLeftArrow.setActionCommand(ACTION_COMMAND_SECOND_SHARE);
        secondSlideRightArrow.setActionCommand(ACTION_COMMAND_SECOND_SHARE);
        secondSlideLabel = new JLabel("       ");
        secondSlide = new ImagePanel();
        secondSlideScroller = new JScrollPane(secondSlide);
        secondSlideScroller.setPreferredSize(new Dimension(SCROLLER_WIDTH, 
                                                           SCROLLER_HEIGHT));
        secondSlideScroller.setBorder(new LineBorder(Color.LIGHT_GRAY));
        
        JPanel secondSlideButtonPanel = new JPanel();
        secondSlideButtonPanel.add(createSecondSlideButton);
        secondSlideButtonPanel.add(loadSecondSlideButton);
        secondSlideButtonPanel.add(saveSecondSlideButton);
        JPanel secondSlideArrowPanel = new JPanel();
        secondSlideArrowPanel.add(secondSlideLeftArrow);
        secondSlideArrowPanel.add(secondSlideLabel);
        secondSlideArrowPanel.add(secondSlideRightArrow);
        secondSlideArrowPanel.setMaximumSize(secondSlideArrowPanel.getPreferredSize());
        JPanel secondSlideScrollerPanel = new JPanel();
        secondSlideScrollerPanel.add(secondSlideScroller);
        
        secondSlidePanel.add(secondSlideButtonPanel);
        secondSlidePanel.add(Box.createVerticalGlue());
        secondSlidePanel.add(secondSlideArrowPanel);
        secondSlidePanel.add(secondSlideScrollerPanel);
        rightPanel.add(secondSlidePanel);
        
        /* Initialization of the result panel. */
        resultSlidePanel = new JPanel();
        resultSlidePanel.setPreferredSize(new Dimension(SLIDE_PANEL_WIDTH, 
                                                        SLIDE_PANEL_HEIGHT));
        resultSlidePanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.GRAY), "Resulting Slide"));
        resultSlidePanel.setLayout(new BoxLayout(resultSlidePanel, 
                                                 BoxLayout.Y_AXIS));
        computeResultButton = new JButton("Overlay Slides");
        saveResultSlideButton = new JButton("Save Slide");
        saveResultSlideButton.setActionCommand(ACTION_COMMAND_RESULT_SHARE);
        JPanel checkBoxPanel = new JPanel();
        checkBoxPanel.setLayout(new GridLayout(2, 2, 0, 0));
        checkBoxPanel.add(new JLabel("Random Slides: "));
        randomSlidesCheckBoxes = new JCheckBox[3];
        JPanel checkBoxPanelOne = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        for(int i = 0; i < 3; i++) {
            randomSlidesCheckBoxes[i] = new JCheckBox("#" + (i+1));
            checkBoxPanelOne.add(randomSlidesCheckBoxes[i]);
        }
        checkBoxPanel.add(checkBoxPanelOne);
        checkBoxPanel.add(new JLabel("Second Slides: "));
        secondSlidesCheckBoxes = new JCheckBox[3];
        JPanel checkBoxPanelTwo = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        for(int i = 0; i < 3; i++) {
            secondSlidesCheckBoxes[i] = new JCheckBox("#" + (i+1));
            checkBoxPanelTwo.add(secondSlidesCheckBoxes[i]);
        }
        checkBoxPanel.add(checkBoxPanelTwo);
        resultSlide = new ImagePanel();
        resultScroller = new JScrollPane(resultSlide);
        resultScroller.setPreferredSize(new Dimension(SCROLLER_WIDTH, 
                                                      SCROLLER_HEIGHT));
        resultScroller.setBorder(new LineBorder(Color.LIGHT_GRAY));
        
        JPanel resultSlideButtonPanel = new JPanel();
        resultSlideButtonPanel.add(computeResultButton);
        resultSlideButtonPanel.add(saveResultSlideButton);
        JPanel resultSlideScrollerPanel = new JPanel();
        resultSlideScrollerPanel.add(resultScroller);
        
        checkBoxPanel.setMaximumSize(new Dimension(SCROLLER_WIDTH, 150));
        
        resultSlidePanel.add(resultSlideButtonPanel);
        resultSlidePanel.add(checkBoxPanel);
        resultSlidePanel.add(resultSlideScrollerPanel);
        
        rightPanel.add(resultSlidePanel);  
    }
    
    /**
     * Returns the currently adjusted superpixel size (size = width = height).
     * 
     * @return the superpixel size.
     */
    public int getSuperpixelSize() {
        return (superpixelSizeSlider.getValue() + 1) * 2;
    } 
    
    /**
     * Sets the displayed image.
     * 
     * @param img the image to be displayed.
     */
    public void setImage(BufferedImage img) {
        image.setImage(img);
    }
    
    /**
     * Sets the displayed random slide.
     * 
     * @param img the slide to be displayed.
     */
    public void setRandomSlide(BufferedImage img) {
        randSlide.setImage(img);
    }
    
    /**
     * Sets the displayed second slide.
     * 
     * @param img the slide to be displayed.
     */
    public void setSecondSlide(BufferedImage img) {
        secondSlide.setImage(img);
    }
    
    /**
     * Sets the displayed overlay slide.
     * 
     * @param img the slide to be displayed.
     */
    public void setResultSlide(BufferedImage img) {
        resultSlide.setImage(img);
    }
    
    /**
     * Sets the load image/slide button listener.
     * 
     * @param al the listener to be set.
     */
    public void setLoadImageButtonListener(ActionListener al) {
        loadImageButton.addActionListener(al);
        loadRandSlideButton.addActionListener(al);
        loadSecondSlideButton.addActionListener(al);
    }
    
    /**
     * Sets the save image/slide button listener.
     * 
     * @param al the listener to be set.
     */
    public void setSaveImageButtonListener(ActionListener al) {
        saveImageButton.addActionListener(al);
        saveRandSlideButton.addActionListener(al);
        saveSecondSlideButton.addActionListener(al);
        saveResultSlideButton.addActionListener(al);
    }
    
    /**
     * Sets the listener for the "create random slide" button.
     * 
     * @param al the listener to be set.
     */
    public void setCreateRandomSlideButtonListener(ActionListener al) {
        createRandSlideButton.addActionListener(al);
    }
    
    /**
     * Sets the listener for the "create second slide" button.
     * 
     * @param al the listener to be set.
     */
    public void setCreateSecondSlideButtonListener(ActionListener al) {
        createSecondSlideButton.addActionListener(al);
    }
    
    /**
     * Sets the listener for the "overlay slides" button.
     * 
     * @param al the listener to be set.
     */
    public void setComputeResultButtonListener(ActionListener al) {
        computeResultButton.addActionListener(al);
    }
    
    /**
     * Sets the listener for the LeftArrowButtons.
     * 
     * @param al the listener to be set.
     */
    public void setLeftArrowButtonListener(ActionListener al) {
        imageLeftArrow.addActionListener(al);
        randSlideLeftArrow.addActionListener(al);
        secondSlideLeftArrow.addActionListener(al);
    }
    
    /**
     * Sets the listener for the RightArrowButtons.
     * 
     * @param al the listener to be set.
     */
    public void setRightArrowButtonListener(ActionListener al) {
        imageRightArrow.addActionListener(al);
        randSlideRightArrow.addActionListener(al);
        secondSlideRightArrow.addActionListener(al);
    }
    
    /**
     * Sets the listener for menu items.
     * 
     * @param al the listener to be set.
     */
    public void setMenuItemListener(ActionListener al) {
        //pixelModeMenuItem.addActionListener(al);
        segmentModeMenuItem.addActionListener(al);
    }
    
    /**
     * Shows the passed message in a dialog window.
     * 
     * @param msg the message to be displayed.
     */
    public void showErrorDialog(String msg) {
        JOptionPane.showMessageDialog(frame, msg);
    }
    
    /**
     * Sets the label of the image panel, including the number of the currently
     * displayed image and the total number of images.
     * 
     * @param currImage the number of the currently displayed image.
     * @param numberOfImages the total number of loaded images.
     */
    public void setImageLabel(int currImage, int numberOfImages) {
        imageLabel.setText(currImage + "/" + numberOfImages);
    }
    
    /**
     * Sets the label of the random slides panel, including the number of the
     * currently displayed slide and the total number of random slides.
     * 
     * @param currSlide the number of the currently displayed slide.
     * @param numberOfSlides the total number of random slides.
     */
    public void setRandomSlideLabel(int currSlide, int numberOfSlides) {
        randomSlideLabel.setText(currSlide + "/" + numberOfSlides);
    }
    
    /**
     * Sets the label of the second slides panel, including the number of the
     * currently displayed slide and the total number of second slides.
     * 
     * @param currSlide the number of the currently displayed slide.
     * @param numberOfSlides the total number of second slides.
     */
    public void setSecondSlideLabel(int currSlide, int numberOfSlides) {
        secondSlideLabel.setText(currSlide + "/" + numberOfSlides);
    }
    
    /**
     * Returns the numbers of all checked random slides.
     * 
     * @return list of all checked random slides.
     */
    public ArrayList<Integer> getCheckedRandomSlides() {
        ArrayList<Integer> randomSlides = new ArrayList();
        for (int i = 0; i < 3; i++) {
            if (randomSlidesCheckBoxes[i].isSelected()) {
                randomSlides.add(i);
            }
        }
        return randomSlides;
    }
    
    /**
     * Returns the numbers of all checked second slides.
     * 
     * @return list of all checked second slides.
     */
    public ArrayList<Integer> getCheckedSecondSlides() {
        ArrayList<Integer> secondSlides = new ArrayList();
        for (int i = 0; i < 3; i++) {
            if (secondSlidesCheckBoxes[i].isSelected()) {
                secondSlides.add(i);
            }
        }
        return secondSlides;
    }
    
    /**
     * Pops up an "Open File" file chooser dialog and returns the selected file.
     * If no file is selected null is returned.
     * 
     * @return the selected file or null.
     */
    public File getFileFromOpenDialog() {
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("Image Files", "png", 
                                                     "gif", "jpg", "jpeg"));
        int status = fc.showOpenDialog(frame);
        if (status == JFileChooser.APPROVE_OPTION) {
            return fc.getSelectedFile();//.getAbsoluteFile();
        }
        return null;
    }
    
    /**
     * Pops up an "Open File" file chooser dialog and returns the selected file.
     * If no file is selected null is returned.
     * 
     * @return the selected file or null.
     */
    public File getFileFromSaveDialog() {
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("PNG Files", "png"));
        fc.setSelectedFile(new File(".png"));
        int status = fc.showSaveDialog(frame);
        if (status == JFileChooser.APPROVE_OPTION) {
            return fc.getSelectedFile();//.getAbsoluteFile();
        }
        return null;
    }
    
    /**
     * Returns the main frame of the GUI.
     * 
     * @return main frame of the GUI.
     */
    public JFrame getFrame() {
        return frame;
    }
    
    /**
     * Creates and shows the frame for segment-based visual cryptography.
     * 
     * @return the created frame.
     */
    public SegmentVCFrame createSegmentFrame() {
        segmentFrame = new SegmentVCFrame("Segment-based visual cryptography - "
                + "VisualCrypto");
        segmentFrame.setVisible(true);
        return segmentFrame;
    }
}