/*
 * This file is part of VisualCrypto.
 * Copyright (C) 2018  Jan Wabbersen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Hashtable;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 * This JFrame contains the GUI for segment-based visual cryptography.
 */
public class SegmentVCFrame extends JFrame {
    
    /**
     * Minimum segment size.
     */
    public static final int MIN_SEGMENT_SIZE = 0;
    /**
     * Maximum segment size.
     */
    public static final int MAX_SEGMENT_SIZE = 3;
    /**
     * Initial segment size.
     */
    public static final int START_SEGMENT_SIZE = 0;
    public static final int SEGMENT_TICK_SPACING = 1;
    
    public static final String ACTION_COMMAND_RANDOM_SHARE = "random_share";
    public static final String ACTION_COMMAND_SECOND_SHARE = "second_share";
    public static final String ACTION_COMMAND_RESULT_SHARE = "result_share";
    
    /**
     * Width of the panel scrollers.
     */
    public static final int SCROLLER_WIDTH = 250;
    /**
     * Height of the panel scrollers.
     */
    public static final int SCROLLER_HEIGHT = 100;
    
    private JPanel leftPanel;
    private JPanel rightPanel;
    
    private JLabel sliderLabel;
    private JSlider segmentSizeSlider;
    private JLabel encodeLabel;
    private JLabel TANLabel;
    private JTextField TANTextField;
    
    private JButton createRandomShareButton;
    private JButton saveRandomShareButton;
    private ImagePanel randomShare;
    private JScrollPane randomShareScroller;
    
    private JButton createSecondShareButton;
    private JButton saveSecondShareButton;
    private ImagePanel secondShare;
    private JScrollPane secondShareScroller;
    
    private JButton createResultShareButton;
    private JButton saveResultShareButton;
    private ImagePanel resultShare;
    private JScrollPane resultShareScroller;
    
    /**
     * Constructor.
     * 
     * @param title the title of the JFrame.
     */
    public SegmentVCFrame(String title) {
        super(title);
        
        initLeftPanel();
        initRightPanel();
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        /* Disable divider of SplitPane. */
        splitPane.setEnabled(false);
        splitPane.setDividerSize(1);
        splitPane.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        splitPane.setLeftComponent(leftPanel);
        splitPane.setRightComponent(rightPanel);
        
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.add(splitPane);
        this.pack();
        this.setResizable(false);
        /* Place the frame in the center of the screen. */
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
    
    /**
     * Helper method to create the left panel.
     */
    private void initLeftPanel() {
        leftPanel = new JPanel();
        leftPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        JPanel settingsPanel = new JPanel();
        settingsPanel.setLayout(new BoxLayout(settingsPanel, BoxLayout.Y_AXIS));
        JLabel settingsLabel = new JLabel("Settings");
        settingsLabel.setFont(new Font("", Font.PLAIN, 20));
        sliderLabel = new JLabel("Segment Size: ");
        sliderLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        segmentSizeSlider = new JSlider(JSlider.HORIZONTAL, MIN_SEGMENT_SIZE, MAX_SEGMENT_SIZE, START_SEGMENT_SIZE);
        segmentSizeSlider.setAlignmentX(Component.LEFT_ALIGNMENT);
        Hashtable<Integer, JLabel> labels = new Hashtable<>();
        labels.put(0, new JLabel("1"));
        labels.put(1, new JLabel("2"));
        labels.put(2, new JLabel("4"));
        labels.put(3, new JLabel("8"));
        segmentSizeSlider.setLabelTable(labels);
        segmentSizeSlider.setPaintLabels(true);
        segmentSizeSlider.setMajorTickSpacing(SEGMENT_TICK_SPACING);
        segmentSizeSlider.setPaintTicks(true);
        
        encodeLabel = new JLabel("TAN To Encode");
        encodeLabel.setFont(new Font("", Font.PLAIN, 20));
        JPanel TANPanel = new JPanel();
        TANPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        TANLabel = new JLabel("TAN (only digits): ");
        TANLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        TANTextField = new JTextField("1234", 4);
        TANPanel.setLayout(new GridLayout(1, 2));
        TANPanel.add(TANLabel);
        TANPanel.add(TANTextField);
        
        settingsPanel.add(settingsLabel);
        settingsPanel.add(Box.createVerticalStrut(20));
        settingsPanel.add(sliderLabel);
        settingsPanel.add(Box.createVerticalStrut(10));
        settingsPanel.add(segmentSizeSlider);
        settingsPanel.add(Box.createVerticalStrut(60));
        settingsPanel.add(encodeLabel);
        settingsPanel.add(Box.createVerticalStrut(10));
        settingsPanel.add(TANPanel);
        leftPanel.add(settingsPanel);
    }
    
    /**
     * Helper method to create the right panel.
     */
    private void initRightPanel() {
        rightPanel = new JPanel();
        rightPanel.setLayout(new GridLayout(3, 1, 10, 10));
        rightPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        
        /* Random Share*/
        JPanel randomSharePanel = new JPanel();
        randomSharePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY), "Random Slide"));
        randomSharePanel.setLayout(new GridLayout(1, 2));
        JPanel firstShareButtonPanel = new JPanel();
        firstShareButtonPanel.setLayout(new BoxLayout(firstShareButtonPanel, BoxLayout.Y_AXIS));
        firstShareButtonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        createRandomShareButton = new JButton("Create Slide");
        saveRandomShareButton = new JButton("Save Slide");
        createRandomShareButton.setActionCommand(ACTION_COMMAND_RANDOM_SHARE);
        saveRandomShareButton.setActionCommand(ACTION_COMMAND_RANDOM_SHARE);
        firstShareButtonPanel.add(createRandomShareButton);
        firstShareButtonPanel.add(Box.createVerticalStrut(5));
        firstShareButtonPanel.add(saveRandomShareButton);
        randomSharePanel.add(firstShareButtonPanel);
        
        randomShare = new ImagePanel();
        randomShareScroller = new JScrollPane(randomShare);
        randomShareScroller.setPreferredSize(new Dimension(SCROLLER_WIDTH, SCROLLER_HEIGHT));
        randomShareScroller.setBorder(new LineBorder(Color.LIGHT_GRAY));
        JPanel randomShareScrollerPanel = new JPanel();
        randomShareScrollerPanel.add(randomShareScroller);
        randomSharePanel.add(randomShareScroller);
        
        rightPanel.add(randomSharePanel);
        
        /* Second Share*/
        JPanel secondSharePanel = new JPanel();
        secondSharePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY), "Seond Slide"));
        secondSharePanel.setLayout(new GridLayout(1, 2));
        JPanel secondShareButtonPanel = new JPanel();
        secondShareButtonPanel.setLayout(new BoxLayout(secondShareButtonPanel, BoxLayout.Y_AXIS));
        secondShareButtonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        createSecondShareButton = new JButton("Create Slide");
        saveSecondShareButton = new JButton("Save Slide");
        createSecondShareButton.setActionCommand(ACTION_COMMAND_SECOND_SHARE);
        saveSecondShareButton.setActionCommand(ACTION_COMMAND_SECOND_SHARE);
        secondShareButtonPanel.add(createSecondShareButton);
        secondShareButtonPanel.add(Box.createVerticalStrut(5));
        secondShareButtonPanel.add(saveSecondShareButton);
        secondSharePanel.add(secondShareButtonPanel);
        
        secondShare = new ImagePanel();
        secondShareScroller = new JScrollPane(secondShare);
        secondShareScroller.setPreferredSize(new Dimension(SCROLLER_WIDTH, SCROLLER_HEIGHT));
        secondShareScroller.setBorder(new LineBorder(Color.LIGHT_GRAY));
        JPanel secondShareScrollerPanel = new JPanel();
        secondShareScrollerPanel.add(secondShareScroller);
        secondSharePanel.add(secondShareScroller);
        
        rightPanel.add(secondSharePanel);
        
        /* Result Share*/
        JPanel resultSharePanel = new JPanel();
        resultSharePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY), "Result Slide"));
        resultSharePanel.setLayout(new GridLayout(1, 2));
        JPanel resultShareButtonPanel = new JPanel();
        resultShareButtonPanel.setLayout(new BoxLayout(resultShareButtonPanel, BoxLayout.Y_AXIS));
        resultShareButtonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        createResultShareButton = new JButton("Overlay Slides");
        saveResultShareButton = new JButton("Save Slide");
        createResultShareButton.setActionCommand(ACTION_COMMAND_RESULT_SHARE);
        saveResultShareButton.setActionCommand(ACTION_COMMAND_RESULT_SHARE);
        resultShareButtonPanel.add(createResultShareButton);
        resultShareButtonPanel.add(Box.createVerticalStrut(5));
        resultShareButtonPanel.add(saveResultShareButton);
        resultSharePanel.add(resultShareButtonPanel);
        
        resultShare = new ImagePanel();
        resultShareScroller = new JScrollPane(resultShare);
        resultShareScroller.setPreferredSize(new Dimension(SCROLLER_WIDTH, SCROLLER_HEIGHT));
        resultShareScroller.setBorder(new LineBorder(Color.LIGHT_GRAY));
        JPanel resultShareScrollerPanel = new JPanel();
        resultShareScrollerPanel.add(resultShareScroller);
        resultSharePanel.add(resultShareScroller);
        
        rightPanel.add(resultSharePanel);
        
    }
    
    /**
     * Sets the displayed random share.
     * @param img the image to be displayed.
     */
    public void setRandomShare(BufferedImage img) {
        randomShare.setImage(img);
    }
    
    /**
     * Sets the displayed second share.
     * @param img the image to be displayed.
     */
    public void setSecondShare(BufferedImage img) {
        secondShare.setImage(img);
    }
    
    /**
     * Sets the displayed overlay share.
     * @param img the image to be displayed.
     */
    public void setResultShare(BufferedImage img) {
        resultShare.setImage(img);
    }
    
    /**
     * Sets the "save share" button listener.
     * 
     * @param al the listener to be set.
     */
    public void setSaveButtonListener(ActionListener al) {
        saveRandomShareButton.addActionListener(al);
        saveSecondShareButton.addActionListener(al);
        saveResultShareButton.addActionListener(al);
    }
    
    /**
     * Sets the "create share" button listener.
     * 
     * @param al the listener to be set.
     */
    public void setCreateButtonListener(ActionListener al) {
        createRandomShareButton.addActionListener(al);
        createSecondShareButton.addActionListener(al);
        createResultShareButton.addActionListener(al);
    }
    
    /**
     * Returns the segment size (width of segments).
     * 
     * @return the segment size.
     */
    public int getSegmentSize() {
        switch (segmentSizeSlider.getValue()) {
            case 0:
                return 1;
            case 1:
                return 2;
            case 2:
                return 4;
            case 3:
                return 8;
        }
        return 1;
    }
    
    /**
     * Returns the entered TAN.
     * 
     * @return the entered TAN.
     */
    public String getTAN() {
        return TANTextField.getText();
    }
    
    /**
     * Shows the passed message in a dialog window.
     * 
     * @param msg the message to be displayed.
     */
    public void showErrorDialog(String msg) {
        JOptionPane.showMessageDialog(this, msg);
    }
    
    /**
     * Pops up an "Open File" file chooser dialog and returns the selected file.
     * If no file is selected null is returned.
     * 
     * @return the selected file or null.
     */
    public File getFileFromSaveDialog() {
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("PNG Files", "png"));
        fc.setSelectedFile(new File(".png"));
        int status = fc.showSaveDialog(this);
        if (status == JFileChooser.APPROVE_OPTION) {
            return fc.getSelectedFile();
        }
        return null;
    }
}