/*
 * This file is part of VisualCrypto.
 * Copyright (C) 2018  Jan Wabbersen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.security.SecureRandom;

/**
 * This class maintains the images, visual cryptography slides and all other
 * visual cryptography related data, and is also responsible for computing the
 * slides.
 * 
 * The shares of the common (pixel-based) visual cryptography are called slides.
 * The shares of the segment-based visual cryptography are called segment shares.
 * There is always a random share (first share) and a second share belonging to
 * a random share.
 */
public class VisualCryptoModel {
    /* --------- Pixel-based VC variables ---------- */
    private final ArrayList<BufferedImage> origImages;
    private final ArrayList<BufferedImage> randomSlides;
    private final ArrayList<BufferedImage> secondSlides;
    private BufferedImage resultSlide;
    
    private int currImage;
    private int currRandomSlide;
    private int currSecondSlide;
    
    /* Stores both superpixels. */
    private BufferedImage[] superPixel;
    /* Factor for the size of the slides relative to the size of the image.
       Can be 2, 4, 6, 8, ... */
    private int superpixelSize;
    private SecureRandom SRNG;
    
    /* --------- Segment-based VC variables ---------- */
    private BufferedImage randomSegmentShare;
    private BufferedImage secondSegmentShare;
    private BufferedImage overlaySegmentShare;
    
    private int segmentSize;
    private int TANLength;
    
    /* All segments and their positions. */
    private static final Segment A1 = new Segment(3, 2, 8, 2);
    private static final Segment A2 = new Segment(3, 0, 8, 0);
    private static final Segment B1 = new Segment(9, 3, 9, 10);
    private static final Segment B2 = new Segment(11, 3, 11, 10);
    private static final Segment C1 = new Segment(9, 14, 9, 21);
    private static final Segment C2 = new Segment(11, 14, 11, 21);
    private static final Segment D1 = new Segment(3, 22, 8, 22);
    private static final Segment D2 = new Segment(3, 24, 8, 24);
    private static final Segment E1 = new Segment(2, 14, 2, 21);
    private static final Segment E2 = new Segment(0, 14, 0, 21);
    private static final Segment F1 = new Segment(2, 3, 2, 10);
    private static final Segment F2 = new Segment(0, 3, 0, 10);
    private static final Segment G1 = new Segment(3, 11, 8, 11);
    private static final Segment G2 = new Segment(3, 13, 8, 13);
    
    private static final ArrayList<Segment> SEGMENTS = new ArrayList<Segment>() {{
        add(A1);
        add(A2);
        add(B1);
        add(B2);
        add(C1);
        add(C2);
        add(D1);
        add(D2);
        add(E1);
        add(E2);
        add(F1);
        add(F2);
        add(G1);
        add(G2);
    }};
    
    /* Digits defined by which segments they are built of. */
    private static final HashSet<Segment> ZERO = new HashSet<>(
            Arrays.asList(A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2));
    private static final HashSet<Segment> ONE = new HashSet<>(
            Arrays.asList(B1, B2, C1, C2));
    private static final HashSet<Segment> TWO = new HashSet<>(
            Arrays.asList(A1, A2, B1, B2, G1, G2, E1, E2, D1, D2));
    private static final HashSet<Segment> THREE = new HashSet<>(
            Arrays.asList(A1, A2, B1, B2, G1, G2, C1, C2, D1, D2));
    private static final HashSet<Segment> FOUR = new HashSet<>(
            Arrays.asList(F1, F2, G1, G2, B1, B2, C1, C2));
    private static final HashSet<Segment> FIVE = new HashSet<>(
            Arrays.asList(A1, A2, F1, F2, G1, G2, C1, C2, D1, D2));
    private static final HashSet<Segment> SIX = new HashSet<>(
            Arrays.asList(A1, A2, F1, F2, E1, E2, D1, D2, C1, C2, G1, G2));
    private static final HashSet<Segment> SEVEN = new HashSet<>(
            Arrays.asList(A1, A2, B1, B2, C1, C2));
    private static final HashSet<Segment> EIGHT = new HashSet<>(
            Arrays.asList(A1, A2, B1, B2, C1, C2, D1, D2, E1, E2, F1, F2, G1, G2));
    private static final HashSet<Segment> NINE = new HashSet<>(
            Arrays.asList(A1, A2, B1, B2, C1, C2, D1, D2, G1, G2, F1, F2));
    
    private static final HashMap<String, HashSet> DIGITTOSET = new HashMap<String, HashSet>() {{
        put("0", ZERO);
        put("1", ONE);
        put("2", TWO);
        put("3", THREE);
        put("4", FOUR);
        put("5", FIVE);
        put("6", SIX);
        put("7", SEVEN);
        put("8", EIGHT);
        put("9", NINE);
        
    }};
    
    /**
     * Constructor - sets initial values.
     */
    public VisualCryptoModel() {
        origImages = new ArrayList<>();
        randomSlides = new ArrayList<>();
        secondSlides = new ArrayList<>();
        currImage = -1;
        currRandomSlide = -1;
        currSecondSlide = -1;
        SRNG = new SecureRandom();
        superpixelSize = 2;
        setSuperPixels();
    }
    
    /**
     * Helper method to create (the two used) superpixels.
     * 
     *   Example with superpixelSize = 4 (x: black, o: white):
     *   superPixel[0]       superPixel[1]
     *   xxoo                ooxx
     *   xxoo                ooxx
     *   ooxx                xxoo
     *   ooxx                xxoo
     */
    private void setSuperPixels() {
        int subPixelWidth = superpixelSize / 2;
        
        superPixel = new BufferedImage[2];
        superPixel[0] = new BufferedImage(superpixelSize, superpixelSize, 
                                          BufferedImage.TYPE_BYTE_BINARY);
        Graphics g = superPixel[0].getGraphics();
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, subPixelWidth, subPixelWidth);
        g.fillRect(subPixelWidth, subPixelWidth, subPixelWidth, subPixelWidth);
        g.setColor(Color.WHITE);
        g.fillRect(subPixelWidth, 0, subPixelWidth, subPixelWidth);
        g.fillRect(0, subPixelWidth, subPixelWidth, subPixelWidth);
        g.dispose();
        
        /* TODO: Use transformation (e.g. rotation) for second superpixel. */
        superPixel[1] = new BufferedImage(superpixelSize, superpixelSize,
                                          BufferedImage.TYPE_BYTE_BINARY);
        Graphics g2 = superPixel[1].getGraphics();
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, subPixelWidth, subPixelWidth);
        g2.fillRect(subPixelWidth, subPixelWidth, subPixelWidth, subPixelWidth);
        g2.setColor(Color.BLACK);
        g2.fillRect(subPixelWidth, 0, subPixelWidth, subPixelWidth);
        g2.fillRect(0, subPixelWidth, subPixelWidth, subPixelWidth);
        g2.dispose();
    }
    
    /**
     * Creates a random slide based on the dimension of the current image and
     * the superpixel size.
     * 
     * This method uses a SecureRandom object for choosing between the two
     * superpixels.
     * It adds the created random slide to {@link VisualCryptoModel#randomSlides}
     * and sets {@link VisualCryptoModel#currRandomSlide} to this new slide.
     * 
     * @throws IllegalStateException if there is no image.
     */
    public void createRandomSlide() throws IllegalStateException {
        /* Check if (the current) image exists. */
        if (currImage == -1 || currImage >= origImages.size()) {
            throw new IllegalStateException("No image!");
        }
        setSuperPixels();
        int width = origImages.get(currImage).getWidth();
        int height = origImages.get(currImage).getHeight();
        BufferedImage randSlide = new BufferedImage(superpixelSize * width,
                                                    superpixelSize * height,
                                                    BufferedImage.TYPE_BYTE_BINARY);
        /* Create graphic context for drawing. */
        Graphics g = randSlide.getGraphics();
        
        /* Go through original image. */
        for (int y = 0; y < height; y++) {
            byte bytes[] = new byte[width];
            SRNG.nextBytes(bytes);
            for (int x = 0; x < width; x++) {
                g.drawImage(superPixel[Math.floorMod(bytes[x], 2)],
                                                     x * superpixelSize,
                                                     y * superpixelSize,
                                                     null);
            }
        }
        g.dispose();
        randomSlides.add(randSlide);
        currRandomSlide = randomSlides.size() - 1;
    }
    
    /**
     * Creates the second slide based on the current image and the current
     * random slide.
     * 
     * It adds the created random slide to {@link VisualCryptoModel#secondSlides}
     * and sets {@link VisualCryptoModel#currSecondSlide} to this new slide.
     * 
     * @throws IllegalStateException if dimensions of the current random slide
     *                               does not mesh with the dimensions of the
     *                               current image according to the superpixel
     *                               size.
     * @throws ArrayIndexOutOfBoundsException if there is no (current) image or
     *                                        random slide.
     */
    public void createSecondSlide() throws IllegalStateException, 
                                           ArrayIndexOutOfBoundsException {
        setSuperPixels();
        BufferedImage origImage = origImages.get(currImage);
        BufferedImage randomSlide = randomSlides.get(currRandomSlide);
        int width = origImage.getWidth();
        int height = origImage.getHeight();
        if (randomSlide.getWidth() != width * superpixelSize ||
            randomSlide.getHeight() != height * superpixelSize) {
            throw new IllegalStateException();
        }
        BufferedImage secondSlide = new BufferedImage(randomSlide.getWidth(), 
                                                      randomSlide.getHeight(),
                                                      BufferedImage.TYPE_BYTE_BINARY);
        Graphics g = secondSlide.getGraphics();
        
        /* Go through original image. */
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                // Use the other superpixel
                if (origImage.getRGB(x, y) == Color.BLACK.getRGB()) {
                    if (randomSlide.getRGB(x * superpixelSize, y * superpixelSize) == 
                            Color.BLACK.getRGB()) {
                        g.drawImage(superPixel[1], x * superpixelSize, 
                                    y * superpixelSize, null);
                    }
                    else {
                        g.drawImage(superPixel[0], x * superpixelSize,
                                    y * superpixelSize, null);
                    }
                }
                // Use the same superpixel
                else {
                    if (randomSlide.getRGB(x * superpixelSize, y * superpixelSize) == 
                            Color.BLACK.getRGB()) {
                        g.drawImage(superPixel[0], x * superpixelSize,
                                    y * superpixelSize, null);
                    }
                    else {
                        g.drawImage(superPixel[1], x * superpixelSize,
                                    y * superpixelSize, null);
                    }
                }
            }
        }
        g.dispose();
        secondSlides.add(secondSlide);
        currSecondSlide = secondSlides.size() - 1;
    }
    
    /**
     * Computes the overlay of the passed slides and saves it in
     * {@link VisualCryptoModel#resultSlide}.
     * 
     * @param randomSlideIndices specifies which random slides will be overlayed.
     * @param secondSlideIndices specifies which second slides will be overlayed.
     * @throws IllegalArgumentException if no slides are passed or slides have
     *                                  different dimensions.
     * @throws IndexOutOfBoundsException if not existent slides are passed.
     */
    public void computeOverlayingSlide(ArrayList<Integer> randomSlideIndices,
                                       ArrayList<Integer> secondSlideIndices)
            throws IllegalArgumentException, IndexOutOfBoundsException {
        ArrayList<BufferedImage> allSlides = new ArrayList();
        for (int slideIndex : randomSlideIndices) {
            allSlides.add(randomSlides.get(slideIndex));
        }
        for (int slideIndex : secondSlideIndices) {
            allSlides.add(secondSlides.get(slideIndex));
        }
        if (allSlides.isEmpty()) {
            throw new IllegalArgumentException("No slides chosen!");
        }
        int width = allSlides.get(0).getWidth();
        int height = allSlides.get(0).getHeight();
        /* Check if all slides have the same dimension. */
        for (BufferedImage slide : allSlides) {
            if (slide.getWidth() != width || slide.getHeight() != height) {
                throw new IllegalArgumentException("Selected slides have "
                        + "different dimensions!");
            }
        }
        resultSlide = new BufferedImage(width, height, 
                                        BufferedImage.TYPE_BYTE_BINARY);       
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                Color pixelColor = Color.WHITE;
                for (BufferedImage slide : allSlides) {
                    if (slide.getRGB(x, y) ==Color.BLACK.getRGB()) {
                        pixelColor = Color.BLACK;
                        break;
                    }
                }
                resultSlide.setRGB(x, y, pixelColor.getRGB());
            }
        }
    }
    
    /**
     * Returns the current image.
     * 
     * @return the current image.
     */
    public BufferedImage getCurrImage() {
        return origImages.get(currImage);
    }
    
    /**
     * Returns the current random slide.
     * 
     * @return the current random slide.
     */
    public BufferedImage getCurrRandomSlide() {
        return randomSlides.get(currRandomSlide);
    }
    
    /**
     * Returns the current second slide.
     * 
     * @return the current second slide.
     */
    public BufferedImage getCurrSecondSlide() {
        return secondSlides.get(currSecondSlide);
    }
    
    /**
     * Returns the overlay of the slides.
     * 
     * @return the overlay of the slides.
     */
    public BufferedImage getResultSlide() {
        return resultSlide;
    }
    
    /**
     * Sets the size of the superpixels.
     * 
     * @param size the width and height of a superpixel.
     */
    public void setSuperpixelSize(int size) {
        superpixelSize = size;
    }
    
    /**
     * Returns the number of the current image starting by 1.
     * @return the number of the current image.
     */
    public int getNumberOfCurrImage() {
        return currImage + 1;
    }
    
    /**
     * Returns the number of the current random slide starting by 1.
     * @return the number of the current random slide.
     */
    public int getNumberOfCurrRandomSlide() {
        return currRandomSlide + 1;
    }
    
    /**
     * Returns the number of the current second slide starting by 1.
     * @return the number of the current second slide.
     */
    public int getNumberOfCurrSecondSlide() {
        return currSecondSlide + 1;
    }
    
    /**
     * Sets the current image.
     * @param number the number of the current image starting by 1.
     */
    public void setNumberOfCurrImage(int number) {
        currImage = number - 1;
    }
    
    /**
     * Sets the current randmon slide.
     * @param number the number of the current random slide starting by 1.
     */
    public void setNumberOfCurrRandomSlide(int number) {
        currRandomSlide = number - 1;
    }
    
    /**
     * Sets the current second slide.
     * @param number the number of the current second slide starting by 1.
     */
    public void setNumberOfCurrSecondSlide(int number) {
        currSecondSlide = number - 1;
    }
    
    /**
     * Returns the number of images currently stored.
     * 
     * @return the number of images.
     */
    public int getNumberOfImages() {
        return origImages.size();
    }
    
    /**
     * Returns the number of random slides currently stored.
     * 
     * @return the number of random slides.
     */
    public int getNumberOfRandomSlides() {
        return randomSlides.size();
    }
    
    /**
     * Returns the number of second slides currently stored.
     * 
     * @return the number of second slides.
     */
    public int getNumberOfSecondSlides() {
        return secondSlides.size();
    }
    
    /**
     * Reads in the passed file and returns it.
     * 
     * @param file the file to be read.
     * @return image or null if an error occured.
     */
    public BufferedImage readImageFromFile(File file) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(file);
        } catch (IOException ex) {
        }
        return image;
    }
    
    /**
     * Adds an image and increments {@link VisualCryptoModel#currImage} by 1.
     * 
     * @param image the image to be added.
     */
    public void addImage(BufferedImage image) {
        origImages.add(imageToBinaryImage(image));
        currImage++;
    }
    
    /**
     * Adds a random slide and increments
     * {@link VisualCryptoModel#currRandomSlide} by 1.
     * 
     * @param image the image to be added.
     */
    public void addRandomSlide(BufferedImage image) {
        randomSlides.add(imageToBinaryImage(image));
        currRandomSlide++;
    }
    
    /**
     * Adds a second slide and increments
     * {@link VisualCryptoModel#currSecondSlide} by 1.
     * 
     * @param image the image to be added.
     */
    public void addSecondSlide(BufferedImage image) {
        secondSlides.add(imageToBinaryImage(image));
        currSecondSlide++;
    }
    
    /**
     * Converts an image to a binary image.
     * 
     * @param image image to be converted.
     * @return binary image.
     */
    private BufferedImage imageToBinaryImage(BufferedImage image) {
        BufferedImage binImage = new BufferedImage(image.getWidth(), 
                                                   image.getHeight(),
                                                   BufferedImage.TYPE_BYTE_BINARY);
        Graphics g = binImage.getGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
        return binImage;
    }
    
    /* ------------ Segment ----------------- */
    /**
     * Creates a random segment share based on the passed TAN length.
     * 
     * Saves the created segment share in
     * {@link VisualCryptoModel#randomSegmentShare}. No scaling based on the
     * segment size at this point.
     * 
     * @param TANLength the length of the TAN.
     */
    public void createRandomSegmentShare(int TANLength) {
        int height = 45;
        int width = 10 + TANLength * 20; // 12 (digit) + 8 (space between digits)
        randomSegmentShare = new BufferedImage(width, height, 
                                               BufferedImage.TYPE_BYTE_BINARY);
        /* Create graphic context for drawing. */
        Graphics g = randomSegmentShare.getGraphics();
        
        /* Set black background. */
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, width, height);
        
        g.setColor(Color.WHITE);
        int x = 10;
        int y = 10;
        /* Go through the numbers of the TAN. */
        for (int i = 0; i < TANLength; i++) {
            byte bytes[] = new byte[7];
            SRNG.nextBytes(bytes);
            
            for (int s = 0; s < SEGMENTS.size(); s = s+2) {
                Segment seg1 = SEGMENTS.get(s);
                Segment seg2 = SEGMENTS.get(s+1);
                if (Math.floorMod(bytes[s/2], 2) == 0) {
                    g.drawLine(x + seg1.getStartX(), y + seg1.getStartY(), 
                               x + seg1.getEndX(), y + seg1.getEndY());
                }
                else {
                    g.drawLine(x + seg2.getStartX(), y + seg2.getStartY(),
                               x + seg2.getEndX(), y + seg2.getEndY());
                }
            }
            
            /* Move reference pixel. */
            x = x + 20;
        }
        g.dispose();
    }
    
    /**
     * Creates the second segment share based on the passed TAN and the random
     * segment share.
     * 
     * Saves the created segment share in
     * {@link VisualCryptoModel#secondSegmentShare}. No scaling based on the
     * segment size at this point.
     * 
     * @param TAN the TAN to be encrypted.
     * @throws IllegalStateException if there is no random share.
     */
    public void createSecondSegmentShare(String TAN) throws IllegalStateException {
        // TODO: Check here for only numbers -> Exception -> Controller forwards error
        
        if (randomSegmentShare == null) {
            throw new IllegalStateException("Random slide is missing.");
        }
        int width = randomSegmentShare.getWidth();
        int height = randomSegmentShare.getHeight();
        secondSegmentShare = new BufferedImage(width, height, 
                                               BufferedImage.TYPE_BYTE_BINARY);
        Graphics g = secondSegmentShare.getGraphics();
        /* Set black background. */
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, width, height);
        
        g.setColor(Color.WHITE);
        int x = 10;
        int y = 10;
        /* Iterate over TAN digit by digit. */
        for (int i = 0; i < TAN.length(); i++) {
            String digit = TAN.substring(i, i+1);
            /* Go through segments (X1, not X2). */
            for (int s = 0; s < SEGMENTS.size(); s = s+2) {
                boolean digitHasSegment = false;
                Segment currSegment = SEGMENTS.get(s);
                if (DIGITTOSET.get(digit).contains(currSegment)) {
                    digitHasSegment = true;
                }
                /* Get color of segment X1 to know if X1 or X2 was chosen in
                   randomSegmentShare. */
                Color segmentX1Color = new Color(randomSegmentShare.getRGB(
                        x + currSegment.getStartX(), y + currSegment.getStartY()));
                if ((segmentX1Color.equals(Color.WHITE) && digitHasSegment) || 
                    (segmentX1Color.equals(Color.BLACK) && !digitHasSegment)) {
                    g.drawLine(x + currSegment.getStartX(), 
                               y + currSegment.getStartY(), 
                               x + currSegment.getEndX(), 
                               y + currSegment.getEndY());
                }
                else {
                    g.drawLine(x + SEGMENTS.get(s+1).getStartX(), 
                               y + SEGMENTS.get(s+1).getStartY(), 
                               x + SEGMENTS.get(s+1).getEndX(), 
                               y + SEGMENTS.get(s+1).getEndY());
                }
            }
            x = x + 20;
        }
        g.dispose();
    }
    
    /**
     * Creates the overlay of the random and the second segment share.
     * 
     * Saves the created segment share in
     * {@link VisualCryptoModel#overlaySegmentShare}.
     */
    public void createOverlaySegmentShare() {
        int width = randomSegmentShare.getWidth();
        int height = randomSegmentShare.getHeight();
        overlaySegmentShare = new BufferedImage(width, height, 
                                                BufferedImage.TYPE_BYTE_BINARY);
        Graphics g = overlaySegmentShare.getGraphics();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (randomSegmentShare.getRGB(x, y) == Color.BLACK.getRGB() || 
                    secondSegmentShare.getRGB(x, y) == Color.BLACK.getRGB()) {
                    g.setColor(Color.BLACK);
                }
                else {
                    g.setColor(Color.WHITE);
                }
                g.drawLine(x, y, x, y);
            }
        }
        g.dispose();
    }
    
    /**
     * Draws and returns an enlarged version of the given binary image according
     * to the given factor.
     * 
     * @param img the image to be enlarged.
     * @param factor the enlargement factor.
     * @return the enlarged image.
     */
    private BufferedImage resizeImage(BufferedImage img, int factor) {
        // TODO: Easier and faster method for resizing needed.
        if (img == null) {
            return null;
        }
        int width = img.getWidth();
        int height = img.getHeight();
        BufferedImage newImg = new BufferedImage(width * factor, height * factor, BufferedImage.TYPE_BYTE_BINARY);
        Graphics g = newImg.getGraphics();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                g.setColor(new Color(img.getRGB(x, y)));
                g.fillRect(x * factor, y * factor, (x + 1)*factor - 1, (y + 1)*factor - 1);
            }
        }
        g.dispose();
        return newImg;
    }
    
    /**
     * Returns the random segment share resized based on the segment size.
     * 
     * @return resized random share.
     */
    public BufferedImage getRandomSegmentShare() {
        return resizeImage(randomSegmentShare, segmentSize);
    }
    
    /**
     * Returns the second segment share resized based on the segment size.
     * 
     * @return resized second share.
     */
    public BufferedImage getSecondSegmentShare() {
        return resizeImage(secondSegmentShare, segmentSize);
    }
    
    /**
     * Returns the overlay of the shares resized based on the segment size.
     * 
     * @return resized random share.
     */
    public BufferedImage getOverlaySegmentShare() {
        return resizeImage(overlaySegmentShare, segmentSize);
    }
    
    /**
     * Sets the segment size to the passed value.
     * 
     * @param size segment size.
     */
    public void setSegmentSize(int size) {
        segmentSize = size;
    }
    
    /**
     * Sets the TAN length to the passed value.
     * 
     * @param length TAN length.
     */
    public void setTANLength(int length) {
        TANLength = length;
    }
    
    /**
     * Returns the TAN length.
     * 
     * @return the TAN length. 
     */
    public int getTANLength() {
        return TANLength;
    }
}

/**
 * A segment is a line defined by to points.
 */
class Segment {
        private int startX;
        private int startY;
        private int endX;
        private int endY;
        
        /**
         * Constructor for Segment.
         * @param startX x coordinate of first end of the segment.
         * @param startY y coordinate of first end of the segment.
         * @param endX x coordinate of the other end of the segment.
         * @param endY y coordinate of the other end of the segment.
         */
        public Segment(int startX, int startY, int endX, int endY) {
            this.startX = startX;
            this.startY = startY;
            this.endX = endX;
            this.endY = endY;
        }
        
        /**
         * Returns x coordinate of the first end of the segment.
         * @return first x coordinate.
         */
        public int getStartX() {
            return startX;
        }
        
        /**
         * Returns y coordinate of the first end of the segment.
         * @return first y coordinate.
         */
        public int getStartY() {
            return startY;
        }
        
        /**
         * Returns x coordinate of the second end of the segment.
         * @return second x coordinate.
         */
        public int getEndX() {
            return endX;
        }
        
        /**
         * Returns y coordinate of the second end of the segment.
         * @return second y coordinate.
         */
        public int getEndY() {
            return endY;
        }
    }