# VisualCrypto

VisualCrypto is a tool to demonstrate pixel-based and segment-based visual cryptography.

## Requirements

Requirements are:

* Java 8

## Run Instructions

Download the executable jar file to any location of your choice from:

[https://gitlab.com/ja.wa/VisualCrypto/raw/master/VisualCrypto.jar](https://gitlab.com/ja.wa/VisualCrypto/raw/master/VisualCrypto.jar)

Run the jar file:

```
java -jar VisualCrypto.jar
```

## Authors

* **Jan Wabbersen** - [ja.wa](https://gitlab.com/ja.wa)

## License

See the [license](license.txt) file.